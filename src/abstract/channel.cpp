#include "abstract/channel.hpp"
#include "main.hpp"



namespace CDLng {
coro<void> DirectChannel::remove() {
    co_await main.api.call(beast::http::verb::delete_, "/channels/"+id.str());
}
}
