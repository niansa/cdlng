#include "abstract/message.hpp"
#include "main.hpp"



namespace CDLng {
coro<void> DirectMessage::remove() {
    co_await main.api.call(beast::http::verb::delete_, "/channels/"+channel_id.str()+"/messages/"+id.str());
}
}
