#include "http.hpp"
#include "main.hpp"

#include <json/json.h>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <string>
#include <string_view>



namespace CDLng {
const std::string api_host = "discord.com",
                  api_version = "8",
                  api_path = "/api/v"+api_version;

APIClient::APIClient(Main& main)
    : resolver(main.ioc), main(main) {}

coro<Json::Value> APIClient::call(beast::http::verb const method, const std::string& path, const Json::Value& data) {
    auto i = main.shared_from_this();

    // These objects perform our I/O
    beast::ssl_stream<beast::tcp_stream> stream(main.ioc, ssl_ctx);

    // Set SNI Hostname (many hosts need this to handshake successfully)
    if(!SSL_set_tlsext_host_name(stream.native_handle(), api_host.c_str())) {
        boost::system::error_code ec;
        ec.assign(static_cast<int>(::ERR_get_error()), asio::error::get_ssl_category());
        std::cerr << ec.message() << "\n";
        throw Exception("Failed to set TLS hostname: "+ec.message());
    }

    // Look up the domain name
    auto results = co_await resolver.async_resolve(api_host, "443", asio::use_awaitable);

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Make the connection on the IP address we get from a lookup
    co_await get_lowest_layer(stream).async_connect(results, asio::use_awaitable);

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Perform the SSL handshake
    co_await stream.async_handshake(asio::ssl::stream_base::client, asio::use_awaitable);

    // Set up an HTTP GET request message
    beast::http::request<beast::http::string_body> req;
    req.version(11);
    req.method(method);
    req.target(api_path+path);
    req.set(beast::http::field::host, api_host);
    req.set(beast::http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    req.set(beast::http::field::authorization, "Bot "+main.settings.bot_token);
    req.set("X-RateLimit-Precision", "millisecond");
    if (!data.isNull()) {
        req.set(beast::http::field::content_type, "application/json");
        req.body() = data.toStyledString();
        req.prepare_payload();
    }

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Send the HTTP request to the remote host
    co_await beast::http::async_write(stream, req, asio::use_awaitable);

    // This buffer is used for reading and must be persisted
    beast::flat_buffer b;

    // This is the object that holds the servers response
    beast::http::response<beast::http::dynamic_body> resp;

    // Receive the HTTP response
    co_await beast::http::async_read(stream, b, resp, asio::use_awaitable);


    // Deserialize body
    Json::Value fres = Json::nullValue;
    if (resp.body().size() != 0) {
        Json::Reader().parse(std::string(reinterpret_cast<const char*>((*resp.body().cdata().begin()).data()), resp.body().size()), fres);
    }

    // Get status code
    auto status = resp.result_int();

    // Check for error
    if (status == 429) {
        // Ratelimit
        main.logger.log(Loglevel::warn, "Ratelimit was hit at "+path);
        co_await main.async_sleep(fres["retry_after"].asFloat()*1000);
        co_return co_await call(method, path, data); // Retry
    } else if (status < 200 || status >= 300) {
        throw Exception("Discord returned error code "+std::to_string(status)+" with the following body:\n"+fres.toStyledString());
    }

    // Set the timeout
    beast::get_lowest_layer(stream).expires_after(timeout);

    // Shutdown stream gracefully
    try {
        co_await stream.async_shutdown(asio::use_awaitable);
    } catch (...) {
        // Exceptions during stream shutdown don't matter... Let's ignore them.
    }

    // Return final result
    co_return fres;
}
}
