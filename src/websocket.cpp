#define BOOST_ASIO_HAS_CO_AWAIT
#include "cdlng.hpp"

#ifndef NDEBUG
#   include <iostream>
#endif
#include <optional>
#include <json/json.h>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/asio/ssl/stream_base.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/beast/websocket/stream.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>



namespace CDLng {
const std::string ws_host = "gateway.discord.gg";


coro<void> WebSocket::heartbeat_send() {
    auto i = main.shared_from_this();

    // Make heartbeat json
    Json::Value json;
    json["op"] = uint(ws_op::heartbeat);
    auto& seq = json["d"];

    // Send heartbeats in a loop
    while (isRunning()) {
        // Wait first
        co_await main.async_sleep(heartbeat_interval);

        // Make json
        if (lastSeqNo < 0) {
            seq = Json::nullValue;
        } else {
            seq = lastSeqNo;
        }

        // Heartbeat
        main.logger.log(Loglevel::verbose, "Heartbeat sent");
        co_await send_json(json);
    }
}

coro<void> WebSocket::connect() {
    unstop();

    // Resolve hostname
    asio::ip::tcp::resolver r(main.ioc);
    auto hosts = co_await r.async_resolve(ws_host, "443", asio::use_awaitable);

    // Check resulution result
    if (hosts.empty()) {
        throw Exception("Failed to resolve WebSocket host");
    }
    endpoint = hosts->endpoint();

    // Connect
    stream = std::make_unique<streamT>(main.ioc, ssl_ctx);
    co_await stream->next_layer().next_layer().async_connect(endpoint, asio::use_awaitable);

    // SSL Handshake
    co_await stream->next_layer().async_handshake(asio::ssl::stream_base::client, asio::use_awaitable);

    // WebSocket configuration
    stream->set_option(
        beast::websocket::stream_base::timeout::suggested(
            beast::role_type::client));

    // Connect to WebSocket
    co_await stream->async_handshake(ws_host, "/?v="+api_version+"&encoding=json", asio::use_awaitable);

    // Get heartbeat interval
    bool need_heartbeat_kickoff = std::exchange(heartbeat_interval, (co_await recv_json())["d"]["heartbeat_interval"].asUInt()) == 0;

    // Kickoff heartbeat
    if (need_heartbeat_kickoff) {
        main.logger.log(Loglevel::debug, "Kicking off heartbeat");
        boost::asio::co_spawn(main.ioc, heartbeat_send(), boost::asio::detached);
    }
}

coro<size_t> WebSocket::send_raw(std::string_view str) {
    main.logger.log(Loglevel::debug, "Client sends ws message");
    main.logger.log(Loglevel::trace, str);
    return stream->async_write(beast::net::buffer(str), asio::use_awaitable);
}

coro<Json::Value> WebSocket::recv_json() {
    Json::Value fres = Json::nullValue;
    beast::flat_buffer buf;
    co_await stream->async_read(buf, asio::use_awaitable);
    auto str = std::string_view{reinterpret_cast<const char*>(buf.cdata().data()), buf.size()};
    main.logger.log(Loglevel::debug, "Client received ws message");;
    main.logger.log(Loglevel::trace, str);
    if (Json::Reader().parse(std::string(str), fres)) {
        auto& s = fres["s"];
        if (s.isInt()) {
            lastSeqNo = s.asInt();
        }
    }
    co_return fres;
}

coro<void> Main::ws_run() {
    auto i = shared_from_this();
    unstop();

    // Authentication data
    std::string auth_raw;
    {
        Json::Value auth = Json::objectValue;
        auth["op"] = uint(ws_op::identify);
        auto& d = auth["d"] = Json::objectValue;
        d["token"] = settings.bot_token;
        d["intents"] = settings.intents;
        auto& p = d["properties"] = Json::objectValue;
        p["$os"] = "linux";
        p["$browser"] = "CDLng";
        p["$device"] = "CDLng";
        auth_raw = auth.toStyledString();
    }

    while (isRunning()) {
        try {
            // Connect
            co_await c.connect();

            // Authenticate
            co_await c.send_raw(auth_raw);

            while (isRunning()) {
                auto data = co_await c.recv_json();
                switch (ws_op(data["op"].asUInt())) {
                case ws_op::heartbeat_ack: {
                    logger.log(Loglevel::verbose, "Heartbeat acknowledgement received");
                } break;
                [[likely]] case ws_op::dispatch: {
                    Json::String test;
                    auto t = data["t"].asString();
                    logger.log(Loglevel::verbose, "Intent received: "+t);
                    boost::asio::co_spawn(ioc, handleIntent(std::move(t), std::move(data["d"])), boost::asio::detached);
                } break;
                [[unlikely]] case ws_op::invalid_session: {
                    throw Exception("Discord reported an invalid session. Is the token valid?");
                } break;
                }
            }
        } catch (std::exception& e) {
            logger.log(Loglevel::error, std::string("Exception in websocket connection: ")+e.what());
        }
    }
}
}
