namespace CDLng {
class Stoppable;
}

#ifndef _STOPPABLE_HPP
#define _STOPPABLE_HPP
namespace CDLng {
/*
 * This class is a little helper to let stuff
 * complete before classes get destroyed.
 * Here is an example on how it's used:
 *
 * unstop(); // Optional, only if you want the
 *           // class to be able to be recovered
 *           // from stopping state
 * auto i = shared_from_this();
 * while (isRunning()) {
 *     ...
 * }
 */
class Stoppable {
private:
    bool stopping = false;

protected:
    void unstop() {
        stopping = false;
    }

public:
    bool isStopping() {
        return stopping;
    }
    bool isRunning() {
        return !stopping;
    }
    void stop() {
        stopping = true;
    }
};

}
#endif
