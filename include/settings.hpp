#ifndef _SETTINGS_HPP
#define _SETTINGS_HPP
#include <string>



namespace CDLng {
/*
 * This class holds some important settings
 */
struct Settings {
    std::string bot_token;
    int intents = 0;
};
}
#endif
