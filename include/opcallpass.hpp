#include <memory>
namespace CDLng {
template<class T>
class CallOpPass;
template<class T>
using AbstractWrapper = CallOpPass<std::shared_ptr<T>>;
}
#ifndef _OPCALLPASS_HPP
#define _OPCALLPASS_HPP
namespace CDLng {
template<class T>
class CallOpPass : public T {
public:
    CallOpPass() : T(nullptr) {}
    CallOpPass(const T& o) : T(o) {}
    CallOpPass(T&& o) : T(o) {}

    template<typename... Args>
    auto& operator()(Args&&... args) const {
        return (**this)(args...);
    }
};
}
#endif
