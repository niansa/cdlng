namespace CDLng {
struct Context;
}

#ifndef _CONTEXT_HPP
#define _CONTEXT_HPP
#include "message.hpp"

#define _CDLNG_CTX_FNC_PASSTHROUGH(o, fnc) template<typename... Args> auto fnc(Args&&... args) {return o->fnc(args...);}



namespace CDLng {
struct Context {
    Message msg;

    _CDLNG_CTX_FNC_PASSTHROUGH(msg, operator())
};
}
#endif
