﻿#include "opcallpass.hpp"
namespace CDLng {
class DirectMessage;
using Message = AbstractWrapper<DirectMessage>;
using const_Message = AbstractWrapper<const DirectMessage>;
}
#ifndef _MESSAGE_HPP
#define _MESSAGE_HPP
#include "base/message.hpp"
#include "abstract/channel.hpp"
#include "ns_base.hpp"
#include "serable.hpp"
#include "snowflake.hpp"



namespace CDLng {
class DirectMessage : public Serable<BaseMessage> {
    Channel channel;

public:
    using Serable<BaseMessage>::Serable;

    virtual coro<void> deep_deserialize(const Json::Value& json) override {
        BaseMessage::deserialize(json);
        co_return;
    }
    virtual coro<Json::Value> deep_serialize() const override {
        co_return BaseMessage::serialize();
    }

    coro<Channel> getChannel() {
        if (!channel) {

        }
        co_return channel;
    }

    coro<void> remove();
};
}
#endif
