namespace CDLng {
struct BaseChannel;
enum class ChannelType;
}
#ifndef _BASE_CHANNEL_HPP
#define _BASE_CHANNEL_HPP
#include "ns_base.hpp"
#include "jsonutils.hpp"

#include <json/json.h>



namespace CDLng {
enum class ChannelType {
    guild_text = 0,
    dm = 1,
    guild_voice = 2,
    group_dm = 3,
    guild_category = 4,
    guild_news = 5,
    guild_store = 6,
    guild_news_thread = 10,
    guild_public_thread = 11,
    guild_private_thread = 12,
    guild_stage_voice = 13
};
enum class ChannelVideoQualityType {
    _auto = 1,
    full = 2,
    _720p = 2
};

struct CreatableChannel {
    std::string name;
    bool nsfw = false;
    ChannelType type = ChannelType::guild_text;
    ChannelVideoQualityType video_quality_mode = ChannelVideoQualityType::_auto;

    void deserialize(const Json::Value& json) {
        jsonGetOv(name, json["name"]);
        jsonGetOv(nsfw, json["nsfw"]);
        jsonGetOv(intRef(type), json["type"]);
        jsonGetOv(intRef(video_quality_mode), json["video_quality_mode"]);
    }
    Json::Value serialize() const {
        Json::Value json = Json::objectValue;
        json["name"] = name;
        json["nsfw"] = nsfw;
        json["type"] = intRef(type);
        json["video_quality_mode"] = intRef(video_quality_mode);
        return json;
    }
};
struct BaseChannel : public CreatableChannel {
    uint member_count = 0,
         message_count = 0;

    void deserialize(const Json::Value& json) {
        CreatableChannel::deserialize(json);
        jsonGetOv(member_count, json["member_count"]);
        jsonGetOv(message_count, json["message_count"]);
    }
    Json::Value serialize() const {
        Json::Value json = CreatableChannel::serialize();
        json["member_count"] = member_count;
        json["message_count"] = message_count;
        return json;
    }
};
}
#endif
