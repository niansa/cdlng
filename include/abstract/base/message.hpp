namespace CDLng {
struct CreatableMessage;
struct BaseMessage;
enum class MessageType;
}
#ifndef _BASE_MESSAGE_HPP
#define _BASE_MESSAGE_HPP
#include "ns_base.hpp"
#include "snowflake.hpp"
#include "jsonutils.hpp"

#include <json/json.h>



namespace CDLng {
enum class MessageType {
    _default = 0,
    recipient_add = 1,
    recipient_remove = 2,
    call = 3,
    channel_name_change = 4,
    channel_icon_change = 5,
    channel_pinned_message = 6,
    guild_member_join = 7,
    user_premium_guild_subscription = 8,
    user_premium_guild_subscription_tier_1 = 9,
    user_premium_guild_subscription_tier_2 = 10,
    user_premium_guild_subscription_tier_3 = 11,
    channel_follow_add = 12,
    guild_discovery_disqualified = 14,
    guild_discovery_requalified = 15,
    guild_discovery_grace_period_initial_warning = 16,
    guild_discovery_grace_period_final_warning = 17,
    thread_created = 18,
    reply = 19,
    application_command = 20,
    thread_starter_message = 21,
    guild_invite_reminder = 22
};

struct CreatableMessage {
    std::string content;
    bool tts = false;

    void deserialize(const Json::Value& json) {
        jsonGetOv(content, json["content"]);
        jsonGetOv(tts, json["tts"]);
    }
    Json::Value serialize() const {
        Json::Value json = Json::objectValue;
        json["content"] = content;
        json["tts"] = tts;
        return json;
    }
};
struct BaseMessage : public CreatableMessage {
    MessageType type = MessageType::_default;
    bool mention_everyone = false,
         pinned = false;
    Snowflake channel_id;

    void deserialize(const Json::Value& json) {
        CreatableMessage::deserialize(json);
        jsonGetOv(intRef(type), json["type"]);
        jsonGetOv(mention_everyone, json["mention_everyone"]);
        jsonGetOv(pinned, json["pinned"]);
        channel_id = json["channel_id"];
    }
    Json::Value serialize() const {
        Json::Value json = CreatableMessage::serialize();
        json["type"] = uint(type);
        json["mention_everyone"] = mention_everyone;
        json["pinned"] = pinned;
        json["channel_id"] = channel_id;
        return json;
    }
};
}
#endif
