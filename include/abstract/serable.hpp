namespace CDLng {
template<class Base>
struct Serable;
class Main; // To not have a cycling include
}

#ifndef _SERABLE_HPP
#define _SERABLE_HPP
#include "ns_base.hpp"
#include "snowflake.hpp"

#include <json/json.h>



namespace CDLng {
template<class Base>
struct Serable : protected Base {
    Main& main;
    Snowflake id;

    Serable(Main& main)
        : main(main) {}

    virtual coro<void> deep_deserialize(const Json::Value& json) {co_return;}
    virtual coro<Json::Value> deep_serialize() const {co_return Json::objectValue;}

    auto deserialize(const Json::Value& json) {
        id = json["id"];
        return deep_deserialize(json);
    }
    coro<Json::Value> serialize() const {
        auto json = co_await deep_serialize();
        json["id"] = id;
        co_return json;
    }

    const Base& operator()() const {
        return *this;
    }
};
}
#endif
