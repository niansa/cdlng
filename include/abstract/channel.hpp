﻿#include "opcallpass.hpp"
namespace CDLng {
class DirectChannel;
using Channel = AbstractWrapper<DirectChannel>;
using const_CHannel = AbstractWrapper<const DirectChannel>;
}
#ifndef _CHANNEL_HPP
#define _CHANNEL_HPP
#include "base/channel.hpp"
#include "ns_base.hpp"
#include "serable.hpp"
#include "snowflake.hpp"



namespace CDLng {
class DirectChannel : public Serable<BaseChannel> {
public:
    using Serable<BaseChannel>::Serable;

    virtual coro<void> deep_deserialize(const Json::Value& json) override {
        BaseChannel::deserialize(json);
        co_return;
    }
    virtual coro<Json::Value> deep_serialize() const override {
        co_return BaseChannel::serialize();
    }

    coro<void> remove();
};
}
#endif
