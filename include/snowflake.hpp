namespace CDLng {
class Snowflake;
}

#ifndef _SNOWFLAKE_HPP
#define _SNOWFLAKE_HPP
#include "ns_base.hpp"

#include <string>
#include <json/value.h>



namespace CDLng {
class Snowflake {
    uint64_t val;

public:
    Snowflake()
        : val(0) {}
    Snowflake(uint64_t value)
        : val(value) {}
    Snowflake(const std::string& value)
        : val(std::stoul(value)) {}
    Snowflake(const Json::Value& value) {
        if (value.isString()) {
            val = std::stoul(value.asString());
        } else {
            val = 0;
        }
    }

    operator std::string()  const {
        return std::to_string(val);
    }
    operator uint64_t() const {
        return val;
    }
    operator Json::Value() const {
        return std::string(*this);
    }
    operator bool() const {
        return val;
    }

    std::string str() const {
        return *this;
    }
    uint64_t uint() const {
        return *this;
    }
    bool empty() const {
        return !*this;
    }
};
}
#endif
