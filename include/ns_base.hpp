#ifndef _NS_BASE_HPP
#define _NS_BASE_HPP
#ifdef CDLNG_CORO_LEGACY_FIX
#define BOOST_ASIO_HAS_STD_COROUTINE
#endif
#define BOOST_ASIO_HAS_CO_AWAIT
#include <string>
#include <stdexcept>
#include <boost/asio/awaitable.hpp>



namespace boost {
namespace asio {}
namespace beast {}
}

namespace CDLng {
struct Exception : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

namespace asio = boost::asio;
namespace beast = boost::beast;

/*
 * These are the details used to connect
 * to the Discord API/WebSocket.
 * Changing these is possible but not
 * recommended unless you exactly know
 * what you are doing.
 * Don't open bug reports with these
 * details changed.
 */
extern const std::string ws_host,
                         api_host,
                         api_version,
                         api_path;

template<typename retT>
using coro = asio::awaitable<retT>;
}
#endif
