namespace CDLng {
class Main;
}

#ifndef _MAIN_HPP
#define _MAIN_HPP
#include "ns_base.hpp"
#include "settings.hpp"
#include "intents.hpp"
#include "logger.hpp"
#include "stoppable.hpp"
#include "websocket.hpp"
#include "http.hpp"

#include <json/json.h>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>
#include <boost/asio/steady_timer.hpp>
#include <memory>



namespace CDLng {
/*
 * This is basically the core of it all:
 * It uses the Connection class to connect
 * to be websocket. It will make REST API
 * calls in the future.
 */
class Main : public std::enable_shared_from_this<Main>, Stoppable {
    friend WebSocket;
    friend APIClient;

    asio::io_context& ioc;
    Logger<Main> logger;

    coro<void> ws_run();
    coro<void> handleIntent(std::string intent, Json::Value json);

public:
    WebSocket c;
    APIClient api;
    IntentHandlers intentHandlers;
    Settings settings;

    Main(asio::io_context& ioc, Settings settings = {}) : ioc(ioc), c(*this), api(*this), settings(std::move(settings)) {
        logger.inst_ptr = this;
    }

    /*
     * Stops all async loops.
     */
    void stop() {
        Stoppable::stop();
        c.stop();
    }
    /*
     * Starts and detaches WebSocket
     * connection.
     */
    void detach() {
        asio::co_spawn(ioc, ws_run(), boost::asio::detached);
    }

    Loglevel& loglevel() {
        return logger.level;
    }

    coro<void> async_sleep(time_t milliseconds) {
        co_await boost::asio::steady_timer(ioc, boost::asio::chrono::milliseconds(milliseconds)).async_wait(asio::use_awaitable);
    }
};
}
#endif
