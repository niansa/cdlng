namespace CDLng {
struct IntentHandlers;
}

#ifndef _INTENTS_HPP
#define _INTENTS_HPP
#include "ns_base.hpp"
#include "abstract.hpp"

#include <functional>
#include <boost/asio/awaitable.hpp>

namespace CDLng {
namespace intents {
    constexpr uint16_t
        none = 0,
        guilds = 1u << 0u,
        guild_members = 1u << 1u,
        guild_bans = 1u << 2u,
        guild_emojis = 1u << 3u,
        guild_integrations = 1u << 4u,
        guild_webhooks = 1u << 5u,
        guild_invites = 1u << 6u,
        guild_voice_states = 1u << 7u,
        guild_presences = 1u << 8u,
        guild_messages = 1u << 9u,
        guild_message_reactions = 1u << 10u,
        guild_message_typing = 1u << 11u,
        direct_messages = 1u << 12u,
        direct_message_reactions = 1u << 13u,
        direct_message_typing = 1u << 14u;
};


/*
 * Intent handling vectors
 */
struct IntentHandlers {
    using coro = asio::awaitable<void>;

    using readyHandler = std::function<coro ()>;

    using memberHandler = std::function<coro ()>;
    using memberHandler_r = std::function<coro ()>;

    using roleHandler = std::function<coro ()>;
    using roleHandler_r = std::function<coro ()>;

    using messageHandler = std::function<coro (Message)>;
    using messageHandler_r = std::function<coro (const_Message)>;

    using channelHandler = std::function<coro ()>;
    using channelHandler_r = std::function<coro ()>;

    using emojiHandler = std::function<coro ()>;

    using banHandler = std::function<coro ()>;
    using banHandler_r = std::function<coro ()>;

    using guildHandler = std::function<coro ()>;
    using guildHandler_r = std::function<coro ()>;

    using presenceHandler = std::function<coro ()>;

    using reactionHandler = std::function<coro ()>;
    using reactionHandler_r = std::function<coro ()>;

    using voiceUpdateHandler = std::function<coro ()>;


    std::vector<readyHandler> ready;
    std::vector<memberHandler> guild_member_add,
                               guild_member_update;
    std::vector<memberHandler_r> guild_member_remove;
    std::vector<roleHandler> guild_role_create,
                             guild_role_update;
    std::vector<roleHandler_r> guild_role_delete;
    std::vector<messageHandler> message_create,
                                message_update,
                                message_remove_all_reactions;
    std::vector<messageHandler_r> message_delete;
    std::vector<channelHandler> channel_create,
                                channel_update,
                                channel_pins_update;
    std::vector<channelHandler_r> channel_delete;
    std::vector<emojiHandler> guild_emojis_update;
    std::vector<banHandler> guild_ban_add;
    std::vector<banHandler_r> guild_ban_remove;
    std::vector<guildHandler> guild_create,
                              guild_update,
                              guild_join;
    std::vector<guildHandler_r> guild_delete;
    std::vector<presenceHandler> presence_update;
    std::vector<reactionHandler> message_reaction_add;
    std::vector<reactionHandler_r> message_reaction_remove;
    std::vector<voiceUpdateHandler> voice_state_update;
};
}
#endif
