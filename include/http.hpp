namespace CDLng {
class APIClient;
class Main; // To avoid some include cycling
}

#ifndef _HTTP_HPP
#define _HTTP_HPP
#include "ns_base.hpp"
#include "settings.hpp"

#include <json/json.h>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/beast/http/verb.hpp>
#include <string>
#include <chrono>
#include <exception>
#include <stdexcept>



namespace CDLng {
class APIClient {
    Main& main;
    boost::asio::ip::tcp::resolver resolver;
    boost::asio::ssl::context ssl_ctx{boost::asio::ssl::context::tlsv12_client};

public:
    struct Exception : public CDLng::Exception {
        using CDLng::Exception::Exception;
    };

    std::chrono::minutes timeout = std::chrono::minutes(2);

    APIClient(Main& main);

    coro<Json::Value> call(beast::http::verb const method, const std::string& path, const Json::Value& data = Json::nullValue);
};
}
#endif
