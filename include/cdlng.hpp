namespace CDLng {
struct Wrapper;
}

#ifndef MAIN_H
#define MAIN_H
#include "main.hpp"

#include <memory>
#include <boost/asio/io_context.hpp>



namespace CDLng {
/*
 * This is a convincience wrapper around
 * the Main class. It sets up the IO Context
 * for you and detaches the Main instance
 * when run().
 */
struct Wrapper {
    boost::asio::io_context ioc;
    std::shared_ptr<Main> main;

    template<typename... Args>
    Wrapper(Args&&... args) {
        main = std::make_shared<CDLng::Main>(ioc, args...);
    }

    /*
     * Gets the underlaying Main
     * class instance.
     */
    auto operator ->() {
        return main.get();
    }

    /*
     * Prepares CDLng and starts
     * the Boost mainloop.
     */
    void run() {
        main->detach();
        ioc.run();
    }
};
}
#endif
