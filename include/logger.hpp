#ifndef _LOGGER_HPP
#define _LOGGER_HPP
#include "magic_enum.hpp"

#include <iostream>
#include <string_view>



namespace CDLng {
enum class Loglevel {
    none,
    error,
    warn,
    info,
    verbose,
    debug,
    trace,
    _default =
#   ifdef NDEBUG
        info
#   else
        debug
#   endif
};


/*
 * This class provides basic logging functionality
 */
template<class InstanceT>
struct Logger {
    Loglevel level = Loglevel::_default;
    InstanceT *inst_ptr;

    void log(Loglevel level, std::string_view message) {
        if (this->level >= level) {
            std::cout << "[" << typeid(InstanceT).name() << '@' << inst_ptr << "] " << magic_enum::enum_name(level) << ": " << message << std::endl;
        }
    }
};

}
#endif
