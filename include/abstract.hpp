#ifndef _ABSTRACT_HPP
#define _ABSTRACT_HPP
#include "abstract/serable.hpp"
#include "abstract/context.hpp"
#include "abstract/message.hpp"
#include "abstract/channel.hpp"
#endif
