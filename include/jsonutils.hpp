#ifndef _JSONUTILS_HPP
#define _JSONUTILS_HPP
#include <memory>
#include <json/json.h>

#include "ns_base.hpp"
#include "abstract/serable.hpp"



namespace CDLng {
template<typename T>
inline int& intRef(T& o) {
    return *reinterpret_cast<int*>(&o);
}
template<typename T>
inline const int& intRef(const T& o) {
    return *reinterpret_cast<const int*>(&o);
}


template<class T>
inline coro<void> jsonDeserOv(std::shared_ptr<T> target, const Json::Value& o) {
    if (o.isObject()) {
        co_await target->deserialize(o);
        co_return true;
    } else {
        co_return false;
    }
}
template<typename T>
inline bool jsonGetOv(T& target, const Json::Value& o) {
    if (o.is<T>()) {
        target = o.as<T>();
        return true;
    } else {
        return false;
    }
}
}
#endif
